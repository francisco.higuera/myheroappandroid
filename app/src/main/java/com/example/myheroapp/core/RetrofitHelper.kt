package com.example.myheroapp.core

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitHelper {
    fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl("https://superheroapi.com/api/3304412732904269/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
}