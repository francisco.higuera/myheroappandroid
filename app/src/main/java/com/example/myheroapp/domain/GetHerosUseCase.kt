package com.example.myheroapp.domain

import com.example.myheroapp.data.HeroRepository
import com.example.myheroapp.data.model.Hero

class GetHerosUseCase {

    private val repository = HeroRepository()

    suspend operator fun invoke():List<Hero>?{
        var heroList: List<Hero> = emptyList()
        for (i in 1..6){
            heroList = repository.getHero()
        }
        return heroList
    }
}