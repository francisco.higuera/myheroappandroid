package com.example.myheroapp.data.model


data class Work(
    val occupation: String,
    val base: String
)