package com.example.myheroapp.data.model

data class ImageHero(
    val url: String
)