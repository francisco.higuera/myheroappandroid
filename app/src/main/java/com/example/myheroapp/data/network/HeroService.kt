package com.example.myheroapp.data.network

import com.example.myheroapp.core.RetrofitHelper
import com.example.myheroapp.data.model.Hero
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class HeroService {

    private val retrofit = RetrofitHelper.getRetrofit()

    suspend fun getHero(id: Int):Hero{

       return withContext(Dispatchers.IO){
            val response = retrofit.create(HeroApiClient::class.java).getHero(id);
             response.body()!!
        }
    }
}