package com.example.myheroapp.data.network

import com.example.myheroapp.data.model.Hero
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path


interface HeroApiClient {

    @GET("{id}")
    suspend fun getHero(@Path("id") id:Int): Response<Hero>
}