package com.example.myheroapp.data.model

import com.google.gson.annotations.SerializedName


data class Hero(
    val id: String,
    val name: String,
    val powerstats: Powerstats,
    val appearance: Appearance,
    val biography: Biography,
    val work: Work,
    val connections: Connections,
    @SerializedName("image") val imageHero: ImageHero

)
