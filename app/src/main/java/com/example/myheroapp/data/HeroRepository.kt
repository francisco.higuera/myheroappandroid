package com.example.myheroapp.data

import com.example.myheroapp.data.model.Hero
import com.example.myheroapp.data.model.provider.HeroProvider
import com.example.myheroapp.data.network.HeroService

class HeroRepository {

    private val apiHero = HeroService()

    suspend fun getHero(): List<Hero> {
        val response = apiHero.getHero(HeroProvider.heros.size + 1)
        HeroProvider.heros += response
        return HeroProvider.heros
    }
}