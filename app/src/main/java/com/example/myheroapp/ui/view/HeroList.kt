package com.example.myheroapp.ui.view

import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.example.myheroapp.R
import com.example.myheroapp.databinding.FragmentHeroListBinding
import com.example.myheroapp.ui.view.adapters.HeroAdapter
import com.example.myheroapp.ui.viewmodel.HeroViewModel
import com.google.gson.Gson


class HeroList : Fragment() {

    private lateinit var binding: FragmentHeroListBinding
    private val heroViewModel: HeroViewModel by viewModels()

    lateinit var layoutManager: LinearLayoutManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        layoutManager = LinearLayoutManager(context)

    }

    var pastVisiblesItems: Int = 0
    var visibleItemCount: Int = 0
    var totalItemCount: Int = 0
    var flag: Boolean = false
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view: View = inflater.inflate(R.layout.fragment_hero_list, container, false)
        binding = FragmentHeroListBinding.bind(view)

        try {
            binding.rvHeros.layoutManager = layoutManager
            heroViewModel.onCreate()
            heroViewModel.heroModel.observe(this.viewLifecycleOwner, Observer { lista ->
                var gson: Gson = Gson()
                binding.rvHeros.layoutManager = LinearLayoutManager(view.context);
                binding.rvHeros.adapter = HeroAdapter(lista)

                (binding.rvHeros.adapter as HeroAdapter).onItemClick = { heroCard ->

                    var bundle: Bundle = Bundle()
                    bundle = bundleOf("hero" to gson.toJson(heroCard).toString())
                    this.findNavController().navigate(R.id.action_heroList_to_heroDetail, bundle)
                }

                binding.rvHeros.addOnScrollListener(object : RecyclerView.OnScrollListener() {
                    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {

                        layoutManager = recyclerView.layoutManager as LinearLayoutManager
                        if (dy > 0) //check for scroll down
                        {
                            visibleItemCount = layoutManager.childCount
                            totalItemCount = layoutManager.itemCount
                            pastVisiblesItems =
                                layoutManager.findFirstCompletelyVisibleItemPosition()
                            if (!flag) {
                                if ((visibleItemCount + pastVisiblesItems) >= totalItemCount) {
                                    heroViewModel.onCreate()
                                    flag = true

                                    Handler().postDelayed(({
                                        flag = false
                                    }), 3000)
                                }
                            }

                        }
                        super.onScrolled(recyclerView, dx, dy)
                    }
                })
            })

            heroViewModel.isLoading.observe(this.viewLifecycleOwner, Observer {
                binding.prgressbar.isVisible = it
            })

        } catch (ex: Throwable) {
            System.out.println(ex.message)
        }

        return view
    }
}