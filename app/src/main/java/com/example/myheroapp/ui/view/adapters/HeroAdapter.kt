package com.example.myheroapp.ui.view.adapters

import android.media.Image
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myheroapp.R
import com.example.myheroapp.data.model.Hero
import com.example.myheroapp.ui.view.HeroList
import com.example.myheroapp.ui.view.MainActivity
import javax.inject.Inject

class HeroAdapter @Inject constructor(private val dataSet: List<Hero>) :
    RecyclerView.Adapter<HeroAdapter.ViewHolder>() {
    var onItemClick: ((Hero) -> Unit)? = null

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val imgHeroImage: ImageView
        val tvHeroName: TextView

        init {
            imgHeroImage = view.findViewById(R.id.heroImage)
            tvHeroName = view.findViewById(R.id.heroName)
            view.setOnClickListener {
                onItemClick?.invoke(dataSet[adapterPosition])
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.hero_card, parent, false)
        )
    }

    override fun getItemCount(): Int {
        return dataSet.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tvHeroName.text = dataSet[position].name
        Glide.with(holder.tvHeroName.context).load(dataSet[position].imageHero.url)
            .into(holder.imgHeroImage);


    }
}