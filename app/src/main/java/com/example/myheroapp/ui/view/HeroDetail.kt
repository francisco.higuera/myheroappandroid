package com.example.myheroapp.ui.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.example.myheroapp.R
import com.example.myheroapp.data.model.Hero
import com.example.myheroapp.databinding.FragmentHeroDetailBinding
import com.google.gson.Gson
import com.google.gson.JsonElement
import dagger.hilt.android.AndroidEntryPoint


class HeroDetail : Fragment() {
    // TODO: Rename and change types of parameters


    private lateinit var binding: FragmentHeroDetailBinding
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val gson: Gson = Gson()
        val view: View = inflater.inflate(R.layout.fragment_hero_detail, container, false)
        var hero: Hero
        binding = FragmentHeroDetailBinding.bind(view)

        arguments.let { bundle ->
            val heroJson: String? = bundle?.getString("hero")
            hero = gson.fromJson(heroJson, Hero::class.java)
            binding.tvHeroName.text = hero.name

            binding.tvGenderHero.text = hero.appearance.gender
            binding.tvWeighttHero.text = hero.appearance.weight[1]
            binding.tvHeightHero.text = hero.appearance.height[1]
            binding.tvEyeColorHero.text = hero.appearance.eyeColor
            binding.tvHairColorHero.text = hero.appearance.hairColor
            binding.tvRacetHero.text = hero.appearance.race

            binding.tvRealHeroName.text = hero.biography.fullName
            binding.tvAlterEgostHero.text = hero.biography.alterEgos
            binding.tvAliasesHero.text = hero.biography.aliases[0]
            binding.tvPlaceOfBirthHero.text = hero.biography.placeOfBirth
            binding.firstAppearanceHero.text = hero.biography.firstAppearance
            binding.tvPublisher.text = hero.biography.publisher
            binding.tvAlignment.text = hero.biography.alignment

            binding.tvInteligenceHero.text = hero.powerstats.intelligence
            binding.tvStrengthHero.text = hero.powerstats.strength
            binding.tvDurabilityHero.text = hero.powerstats.durability
            binding.tvPowerHero.text = hero.powerstats.power
            binding.tvSpeedHero.text = hero.powerstats.speed
            binding.tvCombatHero.text = hero.powerstats.combat

            binding.tvOccupationHerp.text = hero.work.occupation
            binding.tvBaseHero.text = hero.work.base

            binding.tvGroupAffiliationHero.text = hero.connections.groupAffiliation
            binding.tvRelativesHero.text = hero.connections.relatives

            Glide.with(this).load(hero.imageHero.url).into(binding.imageHero);

            binding.btnBackToList.setOnClickListener(View.OnClickListener {
                this.findNavController().navigate(R.id.action_heroDetail_to_heroList)
            })
        }

        return view;
    }

}