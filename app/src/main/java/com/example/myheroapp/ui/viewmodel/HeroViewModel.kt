package com.example.myheroapp.ui.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myheroapp.data.model.Hero
import com.example.myheroapp.domain.GetHerosUseCase
import kotlinx.coroutines.launch

class HeroViewModel: ViewModel() {

    val heroModel = MutableLiveData<List<Hero>>()
    var getHeroUseCase = GetHerosUseCase()
    val isLoading = MutableLiveData<Boolean>()

    fun onCreate(){
        viewModelScope.launch {
            try {
                isLoading.postValue(true)
                val result = getHeroUseCase()
                if(!result.isNullOrEmpty()){
                    heroModel.postValue(result!!)
                    isLoading.postValue(false)
                }
            }catch (ex:Throwable){
                ex.message;
                isLoading.postValue(false)
            }

        }
    }
}